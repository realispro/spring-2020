package stellar.dao;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import stellar.dao.impl.inmemory.InMemorySystemDAO;
import stellar.model.PlanetarySystem;

import org.springframework.context.ApplicationContext;

import java.util.List;

@Configuration
@ComponentScan("stellar.dao")
public class StellarDataStart {

    public static void main(String[] args) {

        System.out.println("StellarDataStart.main");

        ApplicationContext context = new AnnotationConfigApplicationContext(StellarDataStart.class);

        SystemDAO systemDAO = context.getBean(SystemDAO.class);
        List<PlanetarySystem> systems = systemDAO.getAllPlanetarySystems();

        systems.forEach( System.out::println );

    }
}
