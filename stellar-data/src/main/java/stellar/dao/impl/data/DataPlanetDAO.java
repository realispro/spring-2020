package stellar.dao.impl.data;

import org.springframework.data.jpa.repository.JpaRepository;
import stellar.model.Planet;
import stellar.model.PlanetarySystem;

import java.util.List;

public interface DataPlanetDAO extends JpaRepository<Planet, Integer> {

    List<Planet> findBySystem(PlanetarySystem system);
}
