package stellar.dao.impl.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import stellar.model.PlanetarySystem;

import java.util.List;

public interface DataSystemDAO extends JpaRepository<PlanetarySystem, Integer> {

    @Query("select s from PlanetarySystem s where s.name like %?1%")
    List<PlanetarySystem> findByNameLike(String like);
}
