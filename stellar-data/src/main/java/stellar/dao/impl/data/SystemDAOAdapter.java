package stellar.dao.impl.data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
import stellar.dao.SystemDAO;
import stellar.model.PlanetarySystem;

import java.util.List;

@Repository
@Primary
public class SystemDAOAdapter implements SystemDAO {

    @Autowired
    DataSystemDAO dataSystemDAO;

    @Override
    public List<PlanetarySystem> getAllPlanetarySystems() {
        return dataSystemDAO.findAll();
    }

    @Override
    public List<PlanetarySystem> getPlanetarySystemsByName(String like) {
        return dataSystemDAO.findByNameLike(like);
    }

    @Override
    public PlanetarySystem getPlanetarySystem(int id) {
        return dataSystemDAO.findById(id).orElse(null);
    }

    @Override
    public PlanetarySystem addPlanetarySystem(PlanetarySystem system) {
        return dataSystemDAO.save(system);
    }
}
