package stellar.dao.impl.data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
import stellar.dao.PlanetDAO;
import stellar.model.Planet;
import stellar.model.PlanetarySystem;

import java.util.List;

@Repository
@Primary
public class PlanetDAOAdapter implements PlanetDAO {

    @Autowired
    DataPlanetDAO dataPlanetDAO;

    @Override
    public List<Planet> getAllPlanets() {
        return dataPlanetDAO.findAll();
    }

    @Override
    public List<Planet> getPlanetsBySystem(PlanetarySystem system) {
        return dataPlanetDAO.findBySystem(system);
    }

    @Override
    public List<Planet> getPlanetsBySystemAndName(PlanetarySystem system, String like) {
        return null;
    }

    @Override
    public Planet getPlanetById(int id) {
        return dataPlanetDAO.findById(id).orElse(null);
    }

    @Override
    public Planet addPlanet(Planet p) {
        return dataPlanetDAO.save(p);
    }
}
