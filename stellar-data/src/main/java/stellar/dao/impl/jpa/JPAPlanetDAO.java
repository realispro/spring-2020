package stellar.dao.impl.jpa;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
import stellar.dao.PlanetDAO;
import stellar.model.Planet;
import stellar.model.PlanetarySystem;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;


public class JPAPlanetDAO implements PlanetDAO {

    @PersistenceContext(name = "stellarUnit")
    EntityManager em;

    @Override
    public List<Planet> getAllPlanets() {
        return em.createQuery("select p from Planet p").getResultList();
    }

    @Override
    public List<Planet> getPlanetsBySystem(PlanetarySystem system) {
        return em.createQuery("select p from Planet p where p.system=:system")
                .setParameter("system", system).getResultList();
    }

    @Override
    public List<Planet> getPlanetsBySystemAndName(PlanetarySystem system, String like) {
        return null;
    }

    @Override
    public Planet getPlanetById(int id) {
        return em.find(Planet.class, id);
    }

    @Override
    public Planet addPlanet(Planet p) {
        em.persist(p);
        return p;
    }
}
