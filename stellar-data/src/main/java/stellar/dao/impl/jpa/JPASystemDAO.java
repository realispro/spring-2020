package stellar.dao.impl.jpa;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import stellar.dao.SystemDAO;
import stellar.model.PlanetarySystem;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;


public class JPASystemDAO implements SystemDAO {

    @PersistenceContext(name = "stellarUnit")
    EntityManager em;

    // SQL -> HQL -> JPQL
    @Override
    public List<PlanetarySystem> getAllPlanetarySystems() {
        return em.createQuery("select s from PlanetarySystem s").getResultList();
    }

    @Override
    public List<PlanetarySystem> getPlanetarySystemsByName(String like) {
        return em.createQuery("select s from PlanetarySystem s where s.name like :like")
                .setParameter("like", like).getResultList();
    }

    @Override
    public PlanetarySystem getPlanetarySystem(int id) {
        return em.find(PlanetarySystem.class, id);
    }

    // required: reuse or create if missing
    // requires new: always creates new tx
    // mandatory: reuse or exception if missing
    // supports: in tx if exist or non-tx is missing
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public PlanetarySystem addPlanetarySystem(PlanetarySystem system) {
        em.persist(system);
        return system;
    }
}
