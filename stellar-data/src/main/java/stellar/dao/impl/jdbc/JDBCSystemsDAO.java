package stellar.dao.impl.jdbc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
import stellar.dao.SystemDAO;
import stellar.model.PlanetarySystem;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;



public class JDBCSystemsDAO implements SystemDAO {

    public static final Logger logger = Logger.getLogger(JDBCSystemsDAO.class.getName());


    public static final String SELECT_ALL_SYSTEMS = "select ps.id as system_id, ps.details as system_details, " +
            "ps.name as system_name, ps.distance as system_distance, ps.discovery as system_discovery from planetarysystem ps";

    public static final String SELECT_SYSTEMS_BY_NAME = "select ps.id as system_id, ps.details as system_details, "+
                "ps.name as system_name, ps.discovery as system_discovery, ps.distance as system_distance from planetarysystem ps where name like ?";

    public static final String SELECT_SYSTEM_BY_ID = "select ps.id as system_id, ps.details as system_details, "+
            "ps.name as system_name, ps.discovery as system_discovery, ps.distance as system_distance from planetarysystem ps where id=?";



    @Autowired
    DataSource dataSource;


    @Override
    public List<PlanetarySystem> getAllPlanetarySystems() {
        List<PlanetarySystem> systems =
            new ArrayList<>();
        try(Connection con = this.dataSource.getConnection();
            Statement statement = con.createStatement();) {
            ResultSet resultSet = statement.executeQuery(SELECT_ALL_SYSTEMS);
            while (resultSet.next()) {
                systems.add(mapSystem(resultSet));
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, ex.getLocalizedMessage(), ex);
        }
        return systems;
    }

    @Override
    public List<PlanetarySystem> getPlanetarySystemsByName(String like) {
        List<PlanetarySystem> systems =
                new ArrayList<>();
        try(Connection con = this.dataSource.getConnection();
            PreparedStatement prpstm = con.prepareStatement(SELECT_SYSTEMS_BY_NAME)) {
            prpstm.setString(1, "%" + like + "%");
            ResultSet rs = prpstm.executeQuery();
            while (rs.next()) {
                systems.add(mapSystem(rs));
            }

        } catch (Exception ex) {
            logger.log(Level.SEVERE, ex.getLocalizedMessage(), ex);
        }
        return systems;
    }

    @Override
    public PlanetarySystem getPlanetarySystem(int id) {
        PlanetarySystem ps = null;

        try(Connection con = this.dataSource.getConnection();
            PreparedStatement prpstm = con.prepareStatement(SELECT_SYSTEM_BY_ID)) {
            prpstm.setInt(1, id);
            ResultSet rs = prpstm.executeQuery();
            if(rs.next()) {
                ps = mapSystem(rs);
            }

        } catch (Exception ex) {
            logger.log(Level.SEVERE, ex.getLocalizedMessage(), ex);
        }
        return ps;
    }

    @Override
    public PlanetarySystem addPlanetarySystem(PlanetarySystem system) {

        return null;
    }

    private PlanetarySystem mapSystem(ResultSet rs) throws SQLException {
        PlanetarySystem ps = new PlanetarySystem();
        ps.setId(rs.getInt("system_id"));
        ps.setName(rs.getString("system_name"));
        ps.setDistance(rs.getFloat("system_distance"));
        ps.setDiscovery(rs.getDate("system_discovery"));
        try{
            ps.setDetails(rs.getURL("system_details"));
        } catch (SQLException sql) { // ignore, maybe null
        }
        return ps;
    }

}
