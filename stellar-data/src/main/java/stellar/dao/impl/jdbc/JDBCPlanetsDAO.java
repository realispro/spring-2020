package stellar.dao.impl.jdbc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
import stellar.dao.PlanetDAO;
import stellar.model.Planet;
import stellar.model.PlanetarySystem;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


public class JDBCPlanetsDAO implements PlanetDAO {

    public static final Logger logger = Logger.getLogger(JDBCPlanetsDAO.class.getName());


    public static final String SELECT_ALL_PLANETS = "select p.id as planet_id, p.name as planet_name " +
            "from planet p";

    public static final String SELECT_PLANETS_BY_SYSTEM = "select p.id as planet_id, p.name as planet_name " +
            "from planet p where system_id=?";

    public static final String SELECT_PLANETS_BY_SYSTEM_AND_NAME = "select p.id as planet_id, p.name as planet_name " +
            "from planet p where system_id=? and name like ?";

    public static final String SELECT_SYSTEM_BY_ID = "select ps.id as system_id, ps.details as system_details, "+
            "ps.name as system_name from planetary_system ps, ps.discovery as system_discovery where ps.id=?";

    public static final String SELECT_PLANET_BY_ID = "select b.id as book_id, " +
            "b.title as book_title, b.author as book_author, b.cover as book_cover, b.price as book_price," +
            "p.id as publisher_id, p.logoimage as publisher_logo, " +
            "p.name as publisher_name " +
            "from book b, publisher p where b.publisher_id = p.id and b.id = ?";


    @Autowired
    DataSource dataSource;


    private Planet mapPlanet(ResultSet rs) throws SQLException {
        Planet p = new Planet();
        p.setId(rs.getInt("planet_id"));
        p.setName(rs.getString("planet_name"));
        return p;
    }

    @Override
    public List<Planet> getAllPlanets() {
        List<Planet> planets =
                 new ArrayList<>();
        try(Connection con = this.dataSource.getConnection();
            Statement statement = con.createStatement();) {
            ResultSet resultSet = statement.executeQuery(SELECT_ALL_PLANETS);
            while (resultSet.next()) {
                planets.add(mapPlanet(resultSet));
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, ex.getLocalizedMessage(), ex);
        }
        return planets;
    }

    @Override
    public List<Planet> getPlanetsBySystem(PlanetarySystem system) {
        List<Planet> systems =
                new ArrayList<>();
        try(Connection con = this.dataSource.getConnection();
            PreparedStatement prpstm = con.prepareStatement(SELECT_PLANETS_BY_SYSTEM)) {
            prpstm.setInt(1, system.getId());
            ResultSet rs = prpstm.executeQuery();
            while (rs.next()) {
                systems.add(mapPlanet(rs));
            }

        } catch (Exception ex) {
            logger.log(Level.SEVERE, ex.getLocalizedMessage(), ex);
        }
        return systems;
    }

    @Override
    public List<Planet> getPlanetsBySystemAndName(PlanetarySystem system, String like) {
        List<Planet> systems = new ArrayList<>();
        try(Connection con = this.dataSource.getConnection();
            PreparedStatement prpstm = con.prepareStatement(SELECT_PLANETS_BY_SYSTEM_AND_NAME)) {
            prpstm.setInt(1, system.getId());
            prpstm.setString(2, "%" + like + "%");
            ResultSet rs = prpstm.executeQuery();
            while (rs.next()) {
                systems.add(mapPlanet(rs));
            }

        } catch (Exception ex) {
            logger.log(Level.SEVERE, ex.getLocalizedMessage(), ex);
        }
        return systems;
    }

    @Override
    public Planet getPlanetById(int id) {
        return null;
    }

    @Override
    public Planet addPlanet(Planet p) {
        return null;
    }
}
