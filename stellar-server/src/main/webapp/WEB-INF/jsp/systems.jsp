<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<jsp:include page="header.jsp"/>

<span>Planetary Systems</span>

<table>
    <thead>
        <tr>
            <th><spring:message code="system.name"/></th>
            <th><spring:message code="system.star"/></th>
            <th><spring:message code="system.distance"/></th>
            <th><spring:message code="system.discovery"/></th>
        </tr>
    </thead>

    <tbody>
        <c:forEach items="${systems}" var="system">
            <tr>
                <td><a href="./planets?systemId=${system.id}">${system.name}</a></td>
                <td>${system.star}</td>
                <td>${system.distance}</td>
                <td>${system.discovery}</td>
            </tr>
        </c:forEach>
        <tr>
            <td colspan="4"><a href="./addSystem">+</a></td>
        </tr>
    </tbody>
</table>


<jsp:include page="footer.jsp"/>
