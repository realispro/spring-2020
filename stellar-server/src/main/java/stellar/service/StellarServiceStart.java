package stellar.service;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import stellar.model.Planet;

import java.util.List;

@Configuration
@ComponentScan({"stellar.service", "stellar.dao"})
public class StellarServiceStart {

    public static void main(String[] args) {
        System.out.println("Let's explore!");

        ApplicationContext context = new AnnotationConfigApplicationContext(StellarServiceStart.class);

        StellarService service = context.getBean(StellarService.class);

        List<Planet> planets = service.getPlanets(service.getSystemById(1));

        planets.forEach(p-> System.out.println(p));

    }
}
