package stellar.service;

import java.util.Date;
import java.util.List;
import java.util.Random;

public class SloganService {

    private List<String> slogans;

    public String getSlogan(){
        return slogans.get(new Random(new Date().getTime()).nextInt(slogans.size()));
    }

}
