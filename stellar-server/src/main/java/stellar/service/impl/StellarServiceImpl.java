package stellar.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import stellar.dao.PlanetDAO;
import stellar.dao.SystemDAO;
import stellar.model.Planet;
import stellar.model.PlanetarySystem;
import stellar.service.StellarService;

import java.util.List;
import java.util.logging.Logger;

@Service
public class StellarServiceImpl implements StellarService {

    Logger logger = Logger.getLogger(StellarServiceImpl.class.getName());

    @Autowired
    private SystemDAO systemDAO;

    @Autowired
    private PlanetDAO planetDAO;

   /* @Autowired
    PlatformTransactionManager tm;
*/
    @Override
    public List<PlanetarySystem> getSystems() {
        logger.info("fetching all planetary systems");
        List<PlanetarySystem> systems = systemDAO.getAllPlanetarySystems();
        logger.info("found: " + systems.size());
        return systems;
    }

    @Override
    public List<PlanetarySystem> getSystemsByName(String like) {
        logger.info("fetching planetary systems like " + like);
        List<PlanetarySystem> systems =  systemDAO.getPlanetarySystemsByName(like);
        logger.info("found: " + systems.size());
        return systems;
    }

    @Override
    public PlanetarySystem getSystemById(int id) {
        logger.info("fetching planetary system by id " + id);

        PlanetarySystem system = systemDAO.getPlanetarySystem(id);
        //logger.info("found: " + system);
        return  system;
    }

    @Override
    public List<Planet> getPlanets(PlanetarySystem s) {
        logger.info("fetching  planets by system " + s);

        List<Planet> planets = planetDAO.getPlanetsBySystem(s);
        logger.info("found:" + planets.size());
        return planets;
    }

    @Override
    public List<Planet> getPlanets(PlanetarySystem system, String like) {
        return planetDAO.getPlanetsBySystemAndName(system,like);
    }


    @Override
    public Planet getPlanetById(int id) {
        return planetDAO.getPlanetById(id);
    }

    @Override
    public Planet addPlanet(Planet p, PlanetarySystem s) {
        p.setSystem(s);
        return planetDAO.addPlanet(p);
    }

    @Override
    @Transactional
    public PlanetarySystem addPlanetarySystem(PlanetarySystem s) {
       /*TransactionStatus ts = tm.getTransaction(new DefaultTransactionDefinition());
       try {*/
           s = systemDAO.addPlanetarySystem(s);
           /*tm.commit(ts);
       }catch (Exception e){
           tm.rollback(ts);
           throw e;
       }*/
       return s;
    }

    public void setPlanetDAO(PlanetDAO planetDAO) {
        this.planetDAO = planetDAO;
    }

    public void setSystemDAO(SystemDAO systemDAO) {
        this.systemDAO = systemDAO;
    }
}
