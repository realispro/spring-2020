package stellar.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.WebSecurityConfigurer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class StellarSecurityConfigurer extends WebSecurityConfigurerAdapter {

    @Autowired
    private DataSource dataSource;

    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth
                //.jdbcAuthentication()
                //.dataSource(dataSource)
                //.withDefaultSchema();
                .inMemoryAuthentication().withUser("user1").password("{noop}user1").roles("ADMIN");
               // .usersByUsernameQuery("select username, password, enabled"
                //        + " from users where username=?")
                //.authoritiesByUsernameQuery("select username, authority "
                 //       + "from authorities where username=?")
                //.passwordEncoder(NoOpPasswordEncoder.getInstance());
                //.passwordEncoder(new BCryptPasswordEncoder());
    }

    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                //.antMatchers("/systems*").anonymous()
                .antMatchers("/**").hasRole("ADMIN")
                //.anyRequest().permitAll()
                .and()
                .httpBasic()
                .and()
                .logout().logoutSuccessUrl("/login.html");
    }

}
