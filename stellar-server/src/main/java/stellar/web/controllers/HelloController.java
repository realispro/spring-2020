package stellar.web.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HelloController {

    @RequestMapping(method = RequestMethod.GET, path = "/hello")
    public String hello(Model model){
        model.addAttribute("hello_text", "Hey Joe!");

        return "hello";
    }

}
