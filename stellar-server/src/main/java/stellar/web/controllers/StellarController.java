package stellar.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import stellar.model.PlanetarySystem;
import stellar.service.StellarService;
import stellar.web.SystemValidator;
import stellar.web.StellarValidationException;

import java.util.List;
import java.util.logging.Logger;


@Controller
public class StellarController {

    private Logger logger = Logger.getLogger(StellarController.class.getName());

    @Autowired
    private StellarService service;

    @Autowired
    private SystemValidator validator;

    @InitBinder
    void initBinding(WebDataBinder binder){
        binder.setValidator(validator);
    }

    //@RequestMapping(method = GET, path = "/systems")
    // /systems?phrase=text
    // JAX-RS: javax.ws.rs
    @GetMapping("/systems")
    public String getSystems(
            Model model,
            @RequestParam(value = "phrase", required = false) String phrase,
            @RequestHeader(value = "Accept-Language", required = false) String acceptLanguage,
            @CookieValue(value = "JSESSIONID", required = false) String sessionId){

        logger.info("Header Accept-Language:" + acceptLanguage);
        logger.info("Cookie JSESSIONID:" + sessionId);

        List<PlanetarySystem> systems;
        if(phrase!=null && !phrase.trim().isEmpty()){
            systems = service.getSystemsByName(phrase);
        } else {
            systems = service.getSystems();
        }

        model.addAttribute("systems", systems);

        return "systems";
    }

    @GetMapping("/planets")
    public String getPlanetsBySystem(Model model, @RequestParam("systemId") int systemId){
        logger.info("about to get planets of system " + systemId);
        PlanetarySystem system = service.getSystemById(systemId);
        model.addAttribute("planets", service.getPlanets(system));
        model.addAttribute("system", system);
        return "planets";
    }

    @GetMapping("/addSystem")
    public String prepareAddSystem(Model model){
        model.addAttribute("systemForm", new PlanetarySystem());
        return "addSystem";
    }

    @PostMapping("/addSystem")
    public String addSystem(@ModelAttribute("systemForm") @Validated PlanetarySystem system, BindingResult br){

        if(br.hasErrors()){
            //return "addSystem";
            throw new StellarValidationException("system validation exception");
        }

        service.addPlanetarySystem(system);
        return "redirect:/systems";
    }

    @ExceptionHandler(StellarValidationException.class)
    public String handleException(StellarValidationException e, Model model){
        model.addAttribute("error_message", e.getMessage());
        return "error";
    }

}
