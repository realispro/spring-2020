package stellar.web.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import stellar.model.Planet;
import stellar.model.PlanetarySystem;
import stellar.service.StellarService;
import stellar.web.StellarValidationException;
import stellar.web.SystemValidator;

import java.util.List;
import java.util.logging.Logger;

@RestController
@RequestMapping("/webapi")
public class StellarRest {

    private Logger logger = Logger.getLogger(StellarRest.class.getName());

    @Autowired
    private StellarService service;


    @Autowired
    private SystemValidator validator;

    @Autowired
    private MessageSource messageSource;

    @InitBinder
    void initBinding(WebDataBinder binder){
        binder.setValidator(validator);
    }

    @GetMapping("/systems")
    public List<PlanetarySystem> getSystems(){
        logger.info("about to fetch planetary systems");
        return service.getSystems();
    }

    @GetMapping("/systems/{id}")
    public PlanetarySystem getSystemById(@PathVariable("id") int id){
        logger.info("about to fetch system of id " + id);
        return service.getSystemById(id);
    }

    @PostMapping("/systems")
    @ResponseStatus(HttpStatus.CREATED)
    public PlanetarySystem addSystem(@RequestBody @Validated PlanetarySystem system, BindingResult br) {

        if(br.hasErrors()){
            /*String result = "validation error";
            re = new ResponseEntity(result, HttpStatus.BAD_REQUEST);*/
            throw new StellarValidationException("validation error");
        } else {
            logger.info("about to add system: " + system);
            return service.addPlanetarySystem(system);
        }
    }

    @GetMapping("/systems/{id}/planets")
    public List<Planet> getPlanetsBySystem(@PathVariable("id") int systemId){
        logger.info("about to fetch planets of system " + systemId);
        return service.getPlanets(service.getSystemById(systemId));
    }

    @ExceptionHandler(StellarValidationException.class)
    public ResponseEntity handleException(StellarValidationException e){
        return new ResponseEntity(e.getMessage(), HttpStatus.FAILED_DEPENDENCY);
    }


}
