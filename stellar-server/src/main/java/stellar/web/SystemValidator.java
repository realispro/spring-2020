package stellar.web;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import stellar.model.PlanetarySystem;

@Component
public class SystemValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.isAssignableFrom(PlanetarySystem.class);
    }

    @Override
    public void validate(Object o, Errors errors) {
        PlanetarySystem system = (PlanetarySystem) o;

        if(system.getDistance()<0){
            errors.rejectValue("distance", "error.negative");
        }

    }
}
