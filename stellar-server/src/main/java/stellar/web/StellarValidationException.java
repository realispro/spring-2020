package stellar.web;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class StellarValidationException extends RuntimeException{

    public StellarValidationException(String message) {
        super(message);
    }

    public StellarValidationException(String message, Throwable cause) {
        super(message, cause);
    }
}
